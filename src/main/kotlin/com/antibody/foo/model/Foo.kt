package com.antibody.foo.model

import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Table
import java.time.Instant
import javax.validation.constraints.NotEmpty


@Table
class Foo(@Id var id: Int?, @NotEmpty var name: String, var created: Instant?)