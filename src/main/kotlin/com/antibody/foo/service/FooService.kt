package com.antibody.foo.service

import com.antibody.foo.model.Foo
import com.antibody.foo.repository.FooRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.time.Instant

@Service
class FooService {
    @Autowired
    lateinit var repository: FooRepository

    fun getOne(id: Int): Foo? {
        return repository.findById(id).orElse(null)
    }

    fun getAll(): Iterable<Foo> {
        return repository.findAll()
    }

    fun create(foo: Foo): Foo {
        foo.created=Instant.now()
        return repository.save(foo)
    }

    fun delete(id: Int) {
        return repository.deleteById(id)
    }

    fun update(foo: Foo): Foo? {
        val id = foo.id

        if (id == null) {
            return null
        } else {
            val dbFoo = getOne(id)
            if (dbFoo != null) {
                dbFoo.name = foo.name
                repository.save(dbFoo)
            }

            return dbFoo
        }
    }
}