package com.antibody.foo.repository

import com.antibody.foo.model.Foo
import org.springframework.data.repository.CrudRepository

interface FooRepository : CrudRepository<Foo, Int>