package com.antibody.foo.controller

import com.antibody.foo.model.Foo
import com.antibody.foo.service.FooService
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PatchMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/v1")
class FooController {
    private val log: Logger = LoggerFactory.getLogger(FooController::class.java)

    @Autowired
    private lateinit var fooService: FooService

    @GetMapping("/foo")
    fun getAll(): Iterable<Foo> {
        // These are here to test changing log levels at runtime using actuator's /loggers/<name> endpoint.
        // To enable TRACE level debugging for this whole controller, for example:
        // curl -X POST -H 'Content-Type: application/json' -d '{"configuredLevel": "TRACE"}' http://localhost:8080/manage/loggers/com.antibody.foo.controller.FooController
        log.trace("A TRACE Message");
        log.debug("A DEBUG Message");
        log.info("An INFO Message");
        log.warn("A WARN Message");
        log.error("An ERROR Message");

        return fooService.getAll()
    }

    @GetMapping("/foo/{id}")
    fun get(@PathVariable id: Int): Foo? {
        // These are here to test changing log levels at runtime using actuator's /loggers/<name> endpoint.
        // To enable TRACE level debugging for this whole controller, for example:
        // curl -X POST -H 'Content-Type: application/json' -d '{"configuredLevel": "TRACE"}' http://localhost:8080/manage/loggers/com.antibody.foo.controller.FooController
        log.trace("A TRACE Message");
        log.debug("A DEBUG Message");
        log.info("An INFO Message");
        log.warn("A WARN Message");
        log.error("An ERROR Message");

        return fooService.getOne(id)
    }

    @PostMapping("/foo")
    fun post(@RequestBody foo: Foo): Foo {
        return fooService.create(foo)
    }

    @PatchMapping("/foo")
    fun patch(@RequestBody foo: Foo): Foo? {
        return fooService.update(foo)
    }

    @DeleteMapping("/foo/{id}")
    fun delete(@PathVariable id: Int) {
        return fooService.delete(id)
    }
}