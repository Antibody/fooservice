package com.antibody.foo.task

import org.slf4j.Logger
import org.slf4j.LoggerFactory


class Syncer {
    private val log: Logger = LoggerFactory.getLogger(Syncer::class.java)

    fun sync() {
        log.info("Syncing...")
    }
}