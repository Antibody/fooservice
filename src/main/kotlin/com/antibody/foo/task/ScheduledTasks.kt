package com.antibody.foo.task

import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component

@Component
class ScheduledTasks {
    @Scheduled(fixedDelay = 5000)
    fun sync() {
        val syncer = Syncer()
        syncer.sync()
    }
}