-- changeset liquibase: 1
create table if not exists foo (
    id bigint identity primary key,
    name varchar(255),
    created timestamp
);
